if ('function' === typeof importScripts) {
    importScripts(
        'https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js'
    );
    /* global workbox */
    if (workbox) {

        /* injection point for manifest files.  */
        workbox.precaching.precacheAndRoute([
  {
    "url": "075346644f66ad05837ee49376d64186.svg",
    "revision": "075346644f66ad05837ee49376d64186"
  },
  {
    "url": "0b8289b0165d813f72fa53984c95c51c.svg",
    "revision": "0b8289b0165d813f72fa53984c95c51c"
  },
  {
    "url": "0b83207640254ecc78dd68c071ed385e.png",
    "revision": "0b83207640254ecc78dd68c071ed385e"
  },
  {
    "url": "180b62a785d644bcd5351affb41f6afd.png",
    "revision": "180b62a785d644bcd5351affb41f6afd"
  },
  {
    "url": "2069f250f8c1ccfa785b956411327b85.png",
    "revision": "2069f250f8c1ccfa785b956411327b85"
  },
  {
    "url": "2c6166e97d30d21cd07cbb1c33a96be7.png",
    "revision": "2c6166e97d30d21cd07cbb1c33a96be7"
  },
  {
    "url": "488b788eee1f7915032e4f83fa7ae877.png",
    "revision": "488b788eee1f7915032e4f83fa7ae877"
  },
  {
    "url": "android-chrome-192x192.png",
    "revision": "e4fb01d6dd4950ff5aad2166347361c2"
  },
  {
    "url": "android-icon-512x512.png",
    "revision": "318de76fd057c2d1bfc2cb4f8d0fd93b"
  },
  {
    "url": "apple-touch-icon.png",
    "revision": "d3d66316cdd9006902d3e7c8c6f96e79"
  },
  {
    "url": "favicon-16x16.png",
    "revision": "98372c48569381cbf571a739aa9e44ba"
  },
  {
    "url": "favicon-32x32.png",
    "revision": "27b375bd2eafe9a1366a81af155fc889"
  },
  {
    "url": "index.html",
    "revision": "ba3aca6a51c1da71a5383bac25a29bf2"
  },
  {
    "url": "safari-pinned-tab.svg",
    "revision": "da8efc32bd30e5b58c341ac85c546250"
  }
]);

        /* custom cache rules*/
        workbox.routing.registerNavigationRoute('/index.html', {
            blacklist: [/^\/_/, /\/[^\/]+\.[^\/]+$/],
        });

        workbox.routing.registerRoute(
            /\.(?:png|gif|jpg|jpeg)$/,
            workbox.strategies.cacheFirst({
                cacheName: 'images',
                plugins: [
                    new workbox.expiration.Plugin({
                        maxEntries: 60,
                        maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
                    }),
                ],
            })
        );

    } else {
    }
}